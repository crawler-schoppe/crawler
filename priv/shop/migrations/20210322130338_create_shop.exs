defmodule Repos.Shop.Migrations.CreateShop do
  use Ecto.Migration

  def change do
    create table(:shop) do
      add(:shopid, :integer,primary_key: true)
      add(:shop_location, :string)
      add(:username, :string)
      add(:name, :string)
      add(:is_official_shop, :boolean)
    end

    create unique_index(:shop, [:shopid], name: :shop_id_pk_index)
    create unique_index(:shop, [:username])

  end
end
