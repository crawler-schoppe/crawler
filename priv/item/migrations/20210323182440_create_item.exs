defmodule Repos.Item.Migrations.CreateItem do
  use Ecto.Migration

  def change do

    create table(:item) do
      add(:itemid, :bigint)
      add(:price, :bigint)
      add(:price_before_discount, :bigint)
      add(:sold, :integer)
      add(:status, :integer)
      add(:discount, :string)
      add(:name, :string)
      add(:brand, :string)
      add(:item_status, :string)
      add(:shopid, :integer)
    end

    create unique_index(:item, [:itemid])

  end
end
