FROM elixir:alpine

WORKDIR /app

COPY . .

RUN mix local.hex --force && \
    mix local.rebar --force && \
    mix do deps.get, deps.compile

CMD ["iex","-S", "mix"]