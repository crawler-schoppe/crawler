defmodule Crawler.Controller.GetItem do
  import Ecto.Query

  def get_item!(username, limit, offset) when is_integer(limit) and is_integer(offset) do
    query = from i in Item, join: s in Shop, on: i.shopid == s.shopid, where: s.username == ^username, limit: ^limit
    result = Repos.Item.all(query)
    if result != [] do
      %{message: "Query items successfully.", data: result, code: 200}
    else
      resp = Crawler.Task.run(username)
      if resp.code == 200 do
        result = Repos.Item.all(query)
        %{message: "Query items successfully...", data: result, code: 200}
      else
        resp
      end
    end
  end
end