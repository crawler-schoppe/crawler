defmodule Crawler.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  require Logger
  use Application

  @impl true
  @port 80
  def start(_type, _args) do
    children = [
      # Starts a worker by calling: Crawler.Worker.start_link(arg)
      # {Crawler.Worker, arg}
      Repos.Shop, Repos.Item,
      {Plug.Cowboy, scheme: :http, plug: Crawler.Router, options: [port: @port]}
    ]
    Logger.info("Starting application...")


    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Crawler.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
