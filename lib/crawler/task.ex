defmodule Crawler.Task do
  @moduledoc false
  require Logger

  @get_shop_detail "https://shopee.vn/api/v4/shop/get_shop_detail"
  @search_items "https://shopee.vn/api/v2/search_items"
  @get_cookie "https://shopee.vn/api/v0/is_short_url/?path="


  def run(username) do
    shop = Repos.Shop.get_by(Shop, username: username)
    if shop != nil do
      %{code: 200, data: [shop], message: "Query shop successfully"}
    else
      case HTTPoison.get(
             @get_shop_detail <> "?username=#{username}",
             get_header(username)
           ) do
        {:ok, resp} ->
          case resp.status_code do
            200 ->
              data = resp.body
                     |> Poison.decode!(
                          as: %{
                            "data" => %Shop{}
                          }
                        )
              shopInfo = data["data"]
              if shopInfo != nil do
                shopInfo = %{shopInfo | username: username}
                Shop.create_shop(shopInfo, username)
                process_data(shopInfo, 10, 0, 0)
                %{code: 200, data: [shopInfo], message: "Crawling shop"}
              else
                %{code: 400, data: [], message: "Invalid shop"}
              end


            _ -> %{code: resp.status_code, data: []}
          end

        {:error, mess} -> %{code: 400, data: [], message: mess.reason}
      end
    end

  end

  defp process_data(shop = %Shop{}, limit, offset, index) do
    if shop != nil do
      case HTTPoison.get(
             @search_items <> "?match_id=#{shop.shopid}&limit=#{limit}&newest=#{
               offset
             }&order=desc&page_type=shop&version=2",
             get_header(shop.username)
           ) do
        {:ok, resp} ->
          case resp.status_code do
            200 -> data = resp.body
                          |> Poison.decode!(as: %Items{})
                   if data.items != [] do
                     save_data(data)
                     index = index + 1
                     #                :time.sleep(1000)
                     process_data(shop, limit, index * limit, index)
                   else
                   end

            _ -> Logger.error "Error code: " <> resp.status_code
          end

        {:error, mess} -> Logger.error @search_items <> " got error: #{mess.reason}"
      end
    else
      IO.puts "NIL"
    end
  end

  defp save_data(data = %Items{}) do
    Enum.each(
      data.items,
      fn (x) ->
        x
        |> Repos.Item.insert!
      end
    )
  end

  def get_cookies(username) do
    case HTTPoison.get(
           @get_cookie <> username
         ) do
      {:ok, resp} ->
        cookies = resp.headers
        |> Enum.filter(
             fn {k, _} ->
               k == "Set-Cookie"
             end
           )
        cookies =
          for {_, v} <- cookies do
            Enum.at(String.split(v, " "), 0)
          end
        Enum.join(cookies, " ")
    end
  end

  def get_header(username) do
    cookies = get_cookies(username)
    %{
      "x-api-source" => :pc,
      :authority => "shopee.vn",
      "user-agent" =>
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36",
      "if-none-match" => "55b03-34492c4c5bfd7d1ba962f6f87c3f3564",
      :cookie => cookies,
      :referer => "https://shopee.vn/#{username}"
    }
  end

end
