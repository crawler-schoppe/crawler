defmodule Crawler.Router do
  use Plug.Router
  import Crawler.Controller.GetItem
  import Plug.Conn.Query

  plug(Plug.Logger)
  plug(:match)
  plug(:dispatch)

  get "/api-info" do
    #    IO.puts conn
    send_json_resp(conn, 200, %{message: "Hello World"})
  end

  get "/:username" do
    q = conn.query_string
    params = decode(q)
    limit =
      try do
        params["limit"]
        |> String.to_integer
      rescue
        _ -> 1
      end

    offset =
      try do
        params["offset"]
        |> String.to_integer
      rescue
        _ -> 0
      end

    if limit < 0 do
      limit = 1
    else
      if limit > 100 do
        limit = 100
      end
    end

    if offset < 0 do
      offset = 1
    else
      if offset > 100 do
        offset = 100
      end
    end

    res = get_item!(username, limit, offset)
    send_json_resp(conn, res.code, res)
  end

  match _ do
    send_json_resp(conn, 404, %{error: "Not found"})
  end

  def send_json_resp(conn, status, body) do
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(status, Poison.encode!(body))
  end
end