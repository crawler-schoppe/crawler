defmodule Item do
  use Ecto.Schema
  @moduledoc false

  @primary_key {:itemid, :integer, []}
  schema "item" do
    field(:price, :integer)
    field(:shopid, :integer)
    field(:price_before_discount, :integer)
    field(:sold, :integer)
    field(:status, :integer)
    field(:discount, :string)
    field(:name, :string)
    field(:brand, :string)
    field(:item_status, :string)
  end

end
