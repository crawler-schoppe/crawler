defmodule Shop do
  use Ecto.Schema
  import Ecto.Changeset

  @moduledoc false

  schema "shop" do
    field(:shopid, :integer,primary_key: true)
    field(:shop_location, :string)
    field(:username, :string)
    field(:name, :string)
    field(:is_official_shop, :boolean)
  end


  def create_shop(data = %Shop{}, username) do
    Repos.Shop.insert(%Shop{
      :shopid => data.shopid,
      :shop_location => data.shop_location,
      :username => username,
      :name => data.name,
      :is_official_shop => data.is_official_shop,
    })
  end

  def changeset(shop, params \\ %{}) do
    shop
    |> cast(params, [:shopid])
    |> unique_constraint(:shopid)
  end

end
